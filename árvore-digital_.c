#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/*
*   Elaborado por Nicolas Oliveira
*/

/*
*   Defini��o do registro (struct) utilizado para implementar a TRIE
*/

typedef struct TST{

    char key;
    unsigned int value;
    struct TST *esq, *center, *dir;

}TST;

/*
*   Prot�tipo das fun��es e procedimentos utilizados
*/

TST* inserir (TST **raiz, char *chave, unsigned int valor);
void removerNo (TST *raiz);
void removerChave (TST *raiz, char *chave);

/*
*   Fun��o que insere uma chave (um vetor de caracteres, onde � passado o ponteiro para o
*   primeiro elemento), a fun��o tamb�m recebe como parametros o ponteiro do ponteiro de raiz e
*   e um numero inteiro para determinar o final da string
*/

TST* inserir(TST **raiz, char *chave, unsigned int valor){

    assert(raiz);

    if (*raiz == NULL){

        (*raiz) = (TST*)malloc(sizeof(TST));
        if(!raiz) return NULL;

        (*raiz)->key = *chave;
        (*raiz)->value = 0; //assumindo 0 como flag para chave n�o existente
        (*raiz)->esq = (*raiz)->center = (*raiz)->dir = NULL;

    }else if ((*raiz)->key == chave){
        if(*(chave+1) == '\0'){
            (*raiz)->value = valor;
            return *raiz;
        }

        return inserir(&((*raiz)->center), *(chave+1), valor);

    }else if ((*raiz)->key > chave){
        return inserir(&((*raiz)->esq), *(chave+1), valor);
    }else if ((*raiz)->key < chave){
        return inserir(&((*raiz)->dir), *(chave+1), valor);
    }

}

/*
*   Procedimento que remove um n� da arvore, recebe um ponteiro como parametro de entrada;
*   Somente chamada pela fun��o removeChave, onde � definido o ponteiro a ser passado como
*   parametro
*/

void removerNo(TST *raiz){

    assert (raiz);
    free (raiz);
    raiz = NULL;

}

/*
*   Procedimento que remove um chave (string) da arvore, recebe como entrada a raiz da arvore
*   e a string que se deseja remover, caso a string n�o exista da arvore, o procedimento
*   simplesmente retorna para aonde foi chamado
*/

void removerChave(TST *raiz, char *chave){

    assert(raiz);

    if((raiz->esq || raiz ->center || raiz->dir) != NULL){

        if(raiz->value != 0){
            raiz->value = 0;
        }

        if(*(chave+1) != '\0'){
            if(raiz->key == *chave){
                removerChave(raiz, *(chave+1));
                if(raiz->value == 0){
                    removerNo(raiz);
                }
            }
        }else if (raiz->esq == *chave){
            removerChave(raiz->esq, *(chave+1));
        }else if (raiz->dir == *chave){
            removerChave(raiz->dir, *(chave+1));
        }else return;

    }

}

main(){

}
